#!/bin/bash

# Load main variables
source config.yaml

# Define working base URL
DEBIAN_URL="https://cdimage.debian.org/debian-cd/current-live"
ISO_TARGET="debian-${DEBIAN_INSTALLER_TYPE}-${DEBIAN_BASE_VERSION}-${ARCH}-${ENVIRONMENT}.iso"
ISO_URL="$DEBIAN_URL/${ARCH}/iso-hybrid/${ISO_TARGET}"

# Initialise specific global variables
ISO_SIZE=""
MD5SUM=""

# Functions

serial () {

	DATE=`date '+%Y%m%d'`
	echo "$DATE"
}

gather_info () {

	# Get ISO size
	ISO_SIZE="`curl -sIL $ISO_URL 2>/dev/null | grep -i Content-Length | awk '{print $2}' | sed 's/\\r//g'`"

	# Get md5sum for current working ISO
	MD5SUM=$(curl -s ${DEBIAN_URL}/${ARCH}/iso-hybrid/MD5SUMS 2>/dev/null | grep ${ISO_TARGET}| awk '{print $1}')

	echo " Summary"
	echo " ---------------------"
	echo " BUILD:                 $(serial)"
	echo " DEBIAN INSTALLER TYPE: debian-${DEBIAN_INSTALLER_TYPE}"
	echo " DEBIAN BASE VERSION:   ${DEBIAN_BASE_VERSION}"
	echo " ARCH:                  ${ARCH}"
	echo " ENVIRONMENT:           ${ENVIRONMENT}"
	echo " FILENAME:              ${ISO_TARGET}"
	echo " ISO URL:               ${ISO_URL}"
	echo " ISO SIZE:              ${ISO_SIZE}"
	echo " ISO MD5SUM:            ${MD5SUM}"

}

get_iso_chunks () {

	# Download ISO target chunk by chunk
	# ~50M in bytes, suggest limit size file on gitlab.com
	CHUNK=50000000
	BEGIN=0
	END=$((CHUNK - 1))

	BOOL="true"
	i=0

	while [ ${BOOL} == "true" ]; do
		if [ ${END} -le ${ISO_SIZE} ]; then

			echo "Writing [ ${BEGIN}..${END} ] bytes segment to ${ISO_TARGET}.$i part"
			curl --progress-bar -L --range $BEGIN-$END -o ${ISO_TARGET}.$i ${ISO_URL}
			BEGIN=$((END + 1))
			END=$((END + CHUNK))
			i=$((i + 1))
		else
			END=""
			echo "Writing last segment [ ${BEGIN}..${ISO_SIZE} ] to ${ISO_TARGET}.$i part"
			curl --progress-bar -L --range $BEGIN-$END -o ${ISO_TARGET}.$i ${ISO_URL}
			BOOL="false"
		fi
	done
}

reassemble_iso () {

	# Reassemble ISO file
	echo "Reassembling ISO file"
	cat ${ISO_TARGET}.? > ${ISO_TARGET}
}

validate_iso_md5sum () {
	# Check ISO file integrity
	if [ $MD5SUM == $(md5sum ${ISO_TARGET} | awk '{print $1}') ]; then
		echo "The ISO file has been sucessfully downloaded and reassembled"
	else
		echo "There was some error during transmission, the target file is corrupt or there was some calculation error"
	fi
}

# Main program

## Tasks
#  - gather_info
#  - get_iso_chunks
#  - reassemble_iso
#  - validate_iso_md5sum
#

gather_info
get_iso_chunks
reassemble_iso
validate_iso_md5sum

